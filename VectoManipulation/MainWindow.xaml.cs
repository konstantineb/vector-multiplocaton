﻿using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using VectoManipulation.Logic;
using VectoManipulation.Utils;

namespace VectoManipulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void GenerateButton_OnClick(object sender, RoutedEventArgs e)
        {
            MyMatrix  matrix1 = new MyMatrix(4);
            Thread.Sleep(100);
            MyMatrix matrix2 = matrix1.Multipy(2);

            this.MatrixGrid1.ItemsSource = WpfUtils.GetBindable2DArray(matrix1.Matrix);
            this.MatrixGrid2.ItemsSource = WpfUtils.GetBindable2DArray(matrix2.Matrix);
        }

        private void MatrixGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            DataGridTextColumn column = e.Column as DataGridTextColumn;
            Binding binding = column.Binding as Binding;
            binding.Path = new PropertyPath(binding.Path.Path + ".Value");
        }
    }
}
