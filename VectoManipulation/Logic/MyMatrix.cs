﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectoManipulation.Logic
{
    public class MyMatrix
    {

        private readonly int[,] _matrix;

        public MyMatrix(int size)
        {
            this.Size = size;
            this._matrix = new int[size, size];
            this.InitMatirx();
        }

        public MyMatrix(int[,] sourceMatrix)
        {
            this._matrix = sourceMatrix;
            this.Size = sourceMatrix.GetLength(0);
        }


        #region Public Properties

        public int Size { get; private set; }

        public int ColumnCount { get { return this.Matrix.GetLength(0); } }
        public int RowCount { get { return this.Matrix.GetLength(1); } }

        public int[,] Matrix
        {
            get { return this._matrix; }

        }

        #endregion

        #region Public Method

        public MyMatrix Multipy(MyMatrix sourceMatrx)
        {
            if (sourceMatrx.Size != this.Size)
            {
                throw new Exception("Matrix sizes must match.");
            }

            return null;
        }

        public MyMatrix Multipy(int multiplier)
        {

            int[,] result = new int[this.RowCount, this.ColumnCount];

            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                for (int j = 0; j < _matrix.GetLength(1); j++)
                {
                    result[i, j] = this._matrix[i, j] * multiplier;
                }
            }

            MyMatrix resultMatrix = new MyMatrix(result);
            return resultMatrix;

        }

        #endregion

        #region Private Methods

        private void InitMatirx()
        {
            Random rnd = new Random(Environment.TickCount);
            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                for (int j = 0; j < _matrix.GetLength(1); j++)
                {
                    _matrix[i, j] = rnd.Next(0, 10);
                }
            }

        }

        #endregion
    }
}
